# AppWorld2024_robot_test


## To Run

Make a commnent under variables and save   
CI file will fire a pipeline run as it looks for changes to `.gitlab-ci.yml` or any of the `.robot` files   
Once changes are saved and push to repo, click CICD on left pane, you will see pipeline is running   
Click on stage circle icon to view pipeline running   
Once job has completed, view job artifacts on right. Click browse to see report.html


[Screenshot_robot](https://www.youtube.com/watch?v=EWErHud3D1Y)
## Getting started

Helpful examples

Robot Framework [Library Reference](https://robotframework.org/?tab=libraries#resources)

[Selenium Library](https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html)

[Requests Library](https://robocorp.com/docs/development-guide/http/http-examples)

Looping over json data [stackoverflow](https://stackoverflow.com/questions/52053569/parsing-json-in-robot-framework)

# Test Cases

Test cases are built in the robot_tests directory utilizing two files:
- resource.robot
  * call out libraries, keywords to be referenced by test cases
- screenshot.robot
  * this file uses keywords to run test cases
  * can have documentation and tags fields to add add'l info

### resource

Settings
Keywords

## Process

Process Librar allows you to run cli, in this case curl
## Selenium

Using this library capture web page into

Test one:
 - Go to url defined by `${URI}` using browser `${BROWSER}`
 - validate title of page
```
Scrape
    Open Browser   ${URI}    ${BROWSER}
    Title Should Be    Google
    Capture Page Screenshot    screenshot-page.png
```

Test two:
 - same steps as above (webpage and title)
 - click on search text box with name `q`
 - input text `puppies` in the box with name `q`
 - submit form, if no name is passed the first form on page
 - sleep 5 seconds to allow page to load
 - capture screen

```
Navigate
    Open Browser   ${URI}    ${BROWSER}
    Title Should Be    Google
    Click Element      name:q
    Input Text         name:q     puppies
    Submit Form
    sleep    5
    Capture Page Screenshot    screenshot-doggo.png
```

## Requests Library
ReqApi

First Test Case:
 - Create new user
 - rcv status code 201

```
 ${data}=     Create Dictionary   name=morph  job=leader  # create python dictionary and populate variable
    ${papi}=     POST    ${APIURL}                        # POST dictionary variables to api url to insert new names
    Status Should Be     201    ${papi}                   # read return status code 201 created
```

Second Test Case:
 - get user from endpoint
 - verify first_name input equals Janet

```
${gapi}=     GET     https://reqres.in/api/users/2                  #GET request to api endpoint and store in var ${gapi}
Should Be Equal As Strings  Janet  ${gapi.json()}[data][first_name] #Sort json response for key first_name
```
Actual data from api endpoint on GET single user:
```
{"data":{
    "id":2,
    "email":"janet.weaver@reqres.in",
    "first_name":"Janet",
    "last_name":"Weaver",
    "avatar":"https://reqres.in/img/faces/2-image.jpg"},
    "support":{
        "url":"https://reqres.in/#support-heading",
        "text":"To keep ReqRes free, contributions towards server costs are appreciated!"
    }
}
```

## RESTinstance

Helpful [link](https://www.roboscripts.org/libraries/restinstance/) for working with Json Object or array   
[GitHub Repo](https://github.com/asyrjasalo/RESTinstance)

Test Case:   
 - GET user from api endpoint
 - validate status code response is 200
 - print out schema from Output
 - validate string schema from $..first_name is equal to Janet

```
RI Test Cases
    ${ri}=  REST.GET  /api/users/2   # creates return in var ${ri}
    Integer  response status  200    # checks for response code
    Output Schema  response body     # returns the output schema, example below
    String  $..first_name  Janet     # Looks for a String at JSON object first_name and looks for a match of Janet
```

Output from `Output Schema  response body`

```
{
    "type": "object",
    "properties": {
        "data": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "integer",
                    "default": 2
                },
                "email": {
                    "type": "string",
                    "default": "janet.weaver@reqres.in"
                },
                "first_name": {
                    "type": "string",
                    "default": "Janet"
                },
                "last_name": {
                    "type": "string",
                    "default": "Weaver"
                },
                "avatar": {
                    "type": "string",
                    "default": "https://reqres.in/img/faces/2-image.jpg"
                }
            },
            "required": [
                "avatar",
                "email",
                "first_name",
                "id",
                "last_name"
            ]
        },
        "support": {
            "type": "object",
            "properties": {
                "url": {
                    "type": "string",
                    "default": "https://reqres.in/#support-heading"
                },
                "text": {
                    "type": "string",
                    "default": "To keep ReqRes free, contributions towards server costs are appreciated!"
                }
            },
            "required": [
                "text",
                "url"
            ]
        }
    },
    "required": [
        "data",
        "support"
    ]
}
```

## To Add

Ping   
DNS   
CRUD   
