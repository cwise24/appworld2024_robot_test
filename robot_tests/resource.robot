*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           REST  https://reqres.in       #juice-shop.herokuapp.com 
Library           Collections
Library           SeleniumLibrary
Library           Process
Library           RequestsLibrary

*** Variables ***
${URI}            https://www.google.com
${APIURL}         https://reqres.in/api/users
${BROWSER}        headlessfirefox
#${CHROMEDRIVER_PATH}        /usr/lib/chromium/

*** Keywords ***
Bot
    ${statc}    Run Process    curl -I $ https://www.google.com   shell=True    alias=statc
    Should Contain    ${statc.stdout}    HTTP/2 200
    ${result}   Get Process Result    statc    rc=True    stdout=yes    stderr=yes

Open Website
    Open Browser   ${URI}    ${BROWSER}
    Title Should Be    Google

Scrape
    Open Browser   ${URI}    ${BROWSER}
    Title Should Be    Google
    Capture Page Screenshot    screenshot-page.png

Navigate
    Open Browser   ${URI}    ${BROWSER}
    Title Should Be    Google
    Click Element      name:q
    Input Text         name:q     puppies
    Submit Form
    sleep    5
    Capture Page Screenshot    screenshot-doggo.png

ReqLib
    ${resp}=     RequestsLibrary.GET    https://www.google.com 
    Status Should Be    200    ${resp}

Api
    ${data}=     Create Dictionary   name=morph  job=leader
    ${papi}=     RequestsLibrary.POST    ${APIURL} 
    Status Should Be     201    ${papi}
    ${gapi}=     RequestsLibrary.GET     ${APIURL}/2
    Should Be Equal As Strings  Janet  ${gapi.json()}[data][first_name]

RI Test Cases
    REST.GET  /api/users/2    #/api/Challenges
    Integer  response status  200
    #REST.Output Schema  response body
    String  $..first_name  Janet
    #String  $.data[0][name]  Access Log
    [Teardown]  #Output schema

RI Patch 
    REST.PATCH  /api/users/2  { "name": "morph", "job": "f5 se" }
    [Teardown]  Output schema
    String  $.job  f5 se
    #Should Not Be Empty  $.updatedAt

RI Delete
    REST.DELETE  /api/users/2
    Integer  response status  200  202  204

Send XSS
   Open Browser  https://nap.f5demos.vegas/?=<script>alert();</script>  ${BROWSER}
   Wait Until Page Contains  Your support ID is:
   [Teardown]

Post SQI
   ${sqi}=  RequestsLibrary.POST  https://nap.f5demos.vegas/  9999999 UNION SELECT 1,2
   Should Contain  ${sqi.text}  Your support ID is:

Powershell Exec
    [Documentation]  Powershell execution attack signature 200003573-200003575
    Open Browser  https://nap.f5demos.vegas/?=%26%20powershell-WindowStyle%20Hidden%20-encode  ${BROWSER}
    Wait Until Page Contains  Your support ID is:
    [Teardown]
