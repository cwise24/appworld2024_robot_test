*** Settings ***
Documentation     A few tests to show various library capibilities. 
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot
Default Tags      owner-cwise

*** Test Cases ***
Curl 
    Bot
    [Tags]  curl-test

Website
    [Documentation]  Using Selenium Lib to go to site
    Open Website
    Maximize Browser Window
    
Screenshot_Page
    [Documentation]  Using Selenium Lib for screenshot
    Scrape
    [Teardown]    Close Browser

Puppies
    [Documentation]  Using Selenium Lib to navigate and screenshot
    Navigate
    [Teardown]    Close Browser

ReqL
    [Documentation]  Using req library for response code
    ReqLib 

ReqApi
    [Documentation]  Testing API call
    [Tags]  reqres.in 
    Api

RestInstance Test 
    RI Test Cases

Patch w RI
    RI Patch

Del w RI
    RI Delete

XSS Test
    Send XSS

Injection
    Post SQI

PowerShell
    Powershell Exec
